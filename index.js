let express = require('express');
let app = express();

const port = 8000; ;
let router = express.Router();

router.get('/', (req, res) => {
	res.json({ message: 'Hello there!' });
});

app.use('/', router);

app.listen(port);
console.log('Server started on port ' + port);
