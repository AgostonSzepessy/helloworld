#!/bin/sh

CA="$1"
CLIENT_CERT="$2"
CLIENT_KEY="$3"

# Setup environment variables for docker-compose
export DOCKER_CERT_PATH="./certs"
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.2.38:2376"

# Create certificates for docker daemon
mkdir $DOCKER_CERT_PATH
cp "$CA" $DOCKER_CERT_PATH/ca.pem
cp "$CLIENT_CERT" $DOCKER_CERT_PATH/cert.pem
cp "$CLIENT_KEY" $DOCKER_CERT_PATH/key.pem

# Deploy container
docker-compose build
docker-compose down
docker-compose up -d --force-recreate
rm -rf $DOCKER_CERT_PATH

